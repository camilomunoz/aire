# Air quality with mbed OS and K64F

This is a repository for an air queality device using the K64F and mbed OS 5.

Please install [mbed CLI](https://github.com/ARMmbed/mbed-cli#installing-mbed-cli).

## Import the application

From the command-line, import the applications:

```
mbed import https://bitbucket.org/camilomunoz/aire
cd aire
```

### Now compile

Invoke `mbed compile`, and specify the name of your platform and your favorite toolchain (`GCC_ARM`, `ARM`, `IAR`). For example, for the GNU Arm Compiler 6:

```
mbed compile -m K64F -t GCC_ARM
```

### Program your board

1. Connect your mbed device to the computer over USB.
1. Copy the binary file to the mbed device.
1. Press the reset button to start the program.
