#include "mbed.h"
#include "http_request.h"
#include "FATFileSystem.h"
#include "SDBlockDevice.h"
#include "EthernetInterface.h"
#include <string>
#include <math.h>
#include <stdio.h>
#include <sstream>
#include <iostream>
#include <errno.h>

#define         ASCII_CR                    13
#define         MEASUREMENT_PERIOD          30.0
#define         ID_03                       "092117010705,"
#define         ID_SO2                      "081116050515,"
#define         ID_CO                       "041117010433,"
#define         ID_NO2                      "091117010115,"
#define         ID_IAQ                      "081216010737,"


// A struct for the SentiloServer object
typedef struct SentiloServerSt {
    std::string address;        // The Sentilo server address
    std::string providerId;     // Provider's id
    std::string token;          // Authorization token
} SentiloServer;

// Functions declaration
void getGasConcentration(char *gasCon, int *measurement, long *concentration, long *temperature, long *humidity, int *counter);
void getDust(double *pm, int *counter);
void dump_response(HttpResponse *);
void sendObservation(SentiloServer &, std::string, std::string, int *ack);
void return_error(int ret_val);
void errno_error(void* ret_val);
long stringToLong(char *str);
